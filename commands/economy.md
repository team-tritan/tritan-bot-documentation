---
title: Economy Commands
description: 
published: 1
date: 2021-01-17T01:14:13.231Z
tags: 
editor: Dylan James
dateCreated: 2021-01-17T01:14:10.902Z
---

# Economy Commands


This section is still being created so please hang tight :\)

| Command | Description |
| :--- | :--- |
| `*daily` | This command lets you start a daily streak by claiming coins every 24 hours. |
| `*balance` | This command will show you the total coin balance of the message author.  |
| `*richest` | This command will show you the economy leaderboard. |

