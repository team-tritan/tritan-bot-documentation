---
title: Fun Commands
description: 
published: 1
date: 2021-01-17T00:20:04.968Z
tags: 
editor: Dylan James
dateCreated: 2021-01-17T00:17:37.528Z
---

# Fun Commands



| Command | Description |
| :--- | :--- |
| `*concrete` | This command gives you facts about concrete.  |
