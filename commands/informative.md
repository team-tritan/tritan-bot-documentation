---
title: Informative Commands
description: 
published: 1
date: 2021-01-17T01:14:13.231Z
tags: 
editor: Dylan James
dateCreated: 2021-01-17T01:14:10.902Z
---

---
- {} is required, [] is optional, and () is the command alias.
---

# Informative Commands


| Command | Description |
| :--- | :--- |
| `*info` | This command will give you specific information about Tritan Bot as a whole. |
| `*help` | Displays information about all current commands. |
| `*help old` | Displays information about all current commands, and previous commands. |
| `*invite` | This command will give you the invite link for Tritan. |
| `*support` | This command will give you the support server invite url. |
| `*ping` | This command will provide you with the bot's latency. |
| `*uptime` | This command will provide you with the bot's uptime status. |
| `*vote` | This command will show you different websites to upvote Tritan on! |
| `*setprefix {new prefix}` | This command will let you change the prefix of the bot. |
| `*shard` | This command will provide you with what shard your server is on.  |
| `*stats` | This command will provide you with statistics about Tritan Bot. |
| `*developers` | This command will provide you with a list of Tritan's developers and support team.|
| `*dashboard` | This command will provide you with a link to Tritan's dashboard. | 
| `*topcommands` | This command will provide you with a list and number of Tritan's most used commands. |
