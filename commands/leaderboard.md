---
title: Leaderboard Commands
description: 
published: 1
date: 2021-01-17T01:14:13.231Z
tags: 
editor: Dylan James
dateCreated: 2021-01-17T01:14:10.902Z
---

# Leaderboard Commands


This section is still being created so please hang tight :\)

| Command | Description |
| :--- | :--- |
| `*top (leaderboard)` | This command lets you see the top 10 ranked users in your guild. |
| `*rank {optional mention}` | This command will show you your rank card. |
| `*setxp {mention} {amount}` | This command will allow server staff to set someone's XP. |
| `*setrank {mention} {rank}` | This command will allow server staff to set someone's rank. |
| `*topmessages` | This command returns a link to our dashboard to display everyone's message counts in your server. |
