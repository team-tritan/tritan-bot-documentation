---
title: Configuration Commands
description: 
published: 1
date: 2021-01-17T01:14:13.231Z
tags: 
editor: Dylan James
dateCreated: 2021-01-17T01:14:10.902Z
---

# Configuration Commands

The options below will allow you to customize the bot for your guild.

| Command | Description |
| :--- | :--- |
| `*setlogs` | This command will give you instructions on how to turn on Tritan's logging. |
| `*joinleave` | This command will give you instructions on how to turn on Tritan's Join/Leave logging. |
| `*muterole {mention}` | Set the mute role for your guild. |
| `*rankchannel` | Set a channel to enable ranking and leaderboards in your guild. |
