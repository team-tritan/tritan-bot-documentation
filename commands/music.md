---
title: Music Commands
description: 
published: 1
date: 2021-01-17T00:20:04.968Z
tags: 
editor: Dylan James
dateCreated: 2021-01-17T00:17:37.528Z
---

---
description: '{} is required, [] is optional, and () is the command alias.'
---

# Music Commands

| Command | Description |
| :--- | :--- |
| `*play {Song Title or URL}` | This command will play a specific song in the voice channel you are currently in. |


