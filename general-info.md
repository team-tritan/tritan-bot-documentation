---
title: General Information
description: 
published: 1
date: 2021-01-17T00:20:04.968Z
tags: 
editor: undefined
dateCreated: 2021-01-17T00:17:37.528Z
---

# General Information

Tritan is a feature-rich Discord bot built with customization in mind. He comes packaged with a variety of commands and a multitude of settings that can be tailored to your specific needs. His codebase also serves as a base framework to easily create Discord bots of all kinds.

We are always working on adding more commands, so if you have a suggestion run `*support` and tell us!

## Voting and Such:

 
[Vote on Top.gg](https://top.gg/bot/732783297872003114/vote)  
[Vote on Discord Labs](https://bots.discordlabs.org/bot/732783297872003114/vote)  

## Dashboard:

Most of the bot's configuration has been moved to the dashboard for ease of access. Please note that only members with `MANAGE_GUILD` permissions will be able to access your guild's portal.

## Invite & Support:

- [Invite Tritan Bot](https://discord.com/api/oauth2/authorize?client_id=732783297872003114&permissions=8&scope=bot)
- [Support Server](https://tritanbot.xyz/support)
- [Open a Ticket](https://tritanbot.xyz/tickets)

